<?php

use App\Web\Page\HomePage;

return [
  'router' => [
    'app_default'=> [
      'type' => 'page',
      'method' => 'get',
      'controller' => HomePage::class,
      'template' => null,
      'parameters' => []
    ]
  ]
];