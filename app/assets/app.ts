/*
 * Global Application
 */

// Font awesome
import "@fortawesome/fontawesome-free/css/fontawesome.css"
import "@fortawesome/fontawesome-free/css/brands.css"
import "@fortawesome/fontawesome-free/css/solid.css"
import "@fortawesome/fontawesome-free/css/regular.css"

// any CSS you import will output into a single css file (app.css in this case)
// import './styles/app.scss';

// Tailwindcss
import './style/tailwind.css';

// Stimulus Application
import './bootstrap';

// Swiper
// import Swiper, { Navigation, Pagination } from 'swiper';
// import 'swiper/css';
// import 'swiper/css/navigation'
// import 'swiper/css/pagination'

// const swiper = new Swiper('.swiper', {
//   modules: [Navigation, Pagination],
//   direction: 'horizontal',
//   loop: true,
//   pagination: {
//     el: '.swiper-pagination'
//   },
//   navigation: {
//     nextEl: '.swiper-button-next',
//     prevEl: '.swiper-button-prev'
//   },
//   scrollbar: {
//     el: '.swiper-scrollbar'
//   }
// });
