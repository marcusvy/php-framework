# Marcus Project

A simple, fast and secure PHP Framework.

## Purpose

This project bring a new concept of PHP programming with the best practices with a short learning curve and prototyping. Design, Performance and Security is the principles of the application.

## Modules

### Gettting Started

[ ] Setup/Instalation
[ ] Creating Pages
[ ] Routing
[ ] Controllers
[ ] Templates
[ ] Configuration/Env

### The Basics
[ ] Database (Doctrine)
[ ] Forms
[ ] Tests
[ ] Sessions
[ ] Cache
[ ] Logger (Monolog)
[ ] Errors/Debugging (Whoops)

### Architecture
[ ] Requests/Responses
[ ] Kernel
[ ] Services
[ ] Events
[ ] Contracts
[ ] Bundles

### Advanced Topics
[ ] Console
[ ] Mailer
[ ] Validation
[ ] Messaging / Queues
[ ] Notifications
[ ] Serialization
[ ] Translation/i18n

### Security
[ ] Introduction
[ ] Users
[ ] Authentication
[ ] Authorization
[ ] Passwords
[ ] CSRF
[ ] LDAP

### FrontEnd
[ ] Assets
[ ] Webpack
[ ] Parcel
[ ] Vite
[ ] React
[ ] Angular
[ ] Vuejs
[ ] Stimulus
[ ] Bootstrap
[ ] Web Link

### Utilities
[ ] HTTP Client
[ ] Files/Filesystem
[ ] Expression Language
[ ] Locks
[ ] Workflows
[ ] String/Unicode
[ ] UID/UUID
[ ] YAML Parser
[ ] DotEnv

### Production
[ ] Deployment
[ ] Performance
[ ] HTTP Cache
[ ] Cloud