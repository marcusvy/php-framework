<?php

namespace Marcus\Http\Handler;

class HttpHandler
{
  public array $request;
  public array $response;
  public string $route;
  public string $requestUrl;
  public string $requestMethod;
  public array $requestParameters;
  public string $application;
  public array $config;
  public string $template;

  public function handle(){

  }
}
