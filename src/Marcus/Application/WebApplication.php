<?php

namespace Marcus\Application;

use Marcus\Kernel;

class WebApplication
{
  private Kernel $kernel;

  public function __construct(Kernel $kernel)
  {
    $this->kernel = $kernel;
    $this->kernel->setAppMode(Kernel::APP_MODE_WEB);
  }

  public function run()
  {
    $this->kernel->boot();
  }
}
