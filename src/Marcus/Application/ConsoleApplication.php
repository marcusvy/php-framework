<?php

namespace Marcus\Application;

use Marcus\Kernel;
use Symfony\Component\Console\Application;

class ConsoleApplication
{
  private Kernel $kernel;
  private Application $console;

  public function __construct(Kernel $kernel)
  {
    $this->kernel = $kernel;
    $this->kernel->setAppMode(Kernel::APP_MODE_CONSOLE);
    $this->console = new Application('Marcus','1.0');
  }

  public function run()
  {
    $this->kernel->boot();
    $this->console->run();
  }
}
