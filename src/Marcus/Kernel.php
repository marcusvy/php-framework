<?php

namespace Marcus;

use DirectoryIterator;
use Exception;
use PHPUnit\Runner\FileDoesNotExistException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Whoops\Handler\PlainTextHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class Kernel
{
  public const CONFIG_APP_ENV = 'APP_ENV';
  public const CONFIG_APP_DEBUG = 'APP_DEBUG';
  public const CONFIG_APP_PATH = 'APP_PATH';
  public const ENV_PRODUCTION = 'prod';
  public const ENV_DEVELOPMENT = 'dev';
  public const APP_MODE_CONSOLE = 'console';
  public const APP_MODE_WEB = 'web';

  private string $app_mode = self::APP_MODE_CONSOLE;
  private string $app_path = '';
  private string $enviroment = self::ENV_DEVELOPMENT;
  private bool $is_debug = false;
  private array $config = [];
  private Run $errorHandler;

  public function __construct(
    string $app_path,
    string $enviroment = self::ENV_DEVELOPMENT,
    bool $is_debug = false
  ) {
    $this->app_path = $app_path;
    $this->enviroment = $enviroment;
    $this->is_debug = $is_debug;
  }

  private function loadEnv()
  {
    $config = new Dotenv($this->enviroment, $this->is_debug);
    $env_path = realpath(sprintf('%s/.env', $this->app_path));
    if (!file_exists($env_path)) {
      throw new LogicException('Marcus .env file not exist. Try create a .env file in root folder.');
    }
    $config->loadEnv($env_path);
    $this->config = array_merge_recursive($this->config, ['env' => $_ENV]);
  }

  private function loadConfigPackage()
  {

    $yamlParser = new Yaml();
    $config_dir = sprintf('%s/app/config', $this->app_path);
    $di = new RecursiveDirectoryIterator($config_dir);
    $i = new RecursiveIteratorIterator($di);
    foreach ($i as $f) {
      if ($f->getExtension() == 'yaml' || $f->getExtension() == 'yml') {
        $this->loadConfigFromYaml($yamlParser, $f);
      }
      if ($f->getExtension() == 'php') {
        $this->loadConfigFromPhp($f);
      }
    }
  }

  /**
   * Load File from YAML
   * @param Yaml $parser
   * @param SplFileInfo $file
   * @return void
   * @throws ParseException
   */
  private function loadConfigFromYaml(Yaml $parser, SplFileInfo $file)
  {
    $filename = realpath($file->getPathname());
    $config = $parser->parse(file_get_contents($filename));
    $this->config = array_merge_recursive($this->config, $config);
  }

  /**
   * Load config from PHP File that return a array
   * @param SplFileInfo $file
   * @return void
   */
  private function loadConfigFromPhp(SplFileInfo $file)
  {
    $filename = realpath($file->getPathname());
    try {
      $config = include $filename;
      if (!is_array($config)) {
        $msg = "Config file must return a Array or Marcus\Config object. Check this file: '%s' ";
        throw new LogicException(sprintf($msg, $file->getFilename()));
      }
    } catch (Exception $e) {
      $msg = "Trying to load file that not exist: '%s'. Check if is necessary to load this config file.";
      throw new FileDoesNotExistException(sprintf($msg, $file->getFilename()));
    }
    $this->config = array_merge_recursive($this->config, $config);
  }

  private function bootErrorHandler(){
    $this->errorHandler = new Run();
    switch($this->app_mode){
      case self::APP_MODE_CONSOLE:
        $this->errorHandler->appendHandler(new PlainTextHandler());
        break;
      case self::APP_MODE_WEB:
        $this->errorHandler->appendHandler(new PrettyPageHandler());
        break;
    }
  }

  public function boot()
  {
    $this->bootErrorHandler();
    $this->loadEnv();
    $this->loadConfigPackage();
  }

  public function getConfig(): array
  {
    return $this->config;
  }

  /**
   * Get the value of app_mode
   */
  public function getAppMode()
  {
    return $this->app_mode;
  }

  /**
   * Set the value of app_mode
   *
   * @return  self
   */
  public function setAppMode($app_mode)
  {
    $this->app_mode = $app_mode;

    return $this;
  }
}
