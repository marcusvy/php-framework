<?php

namespace MarcusTest;

use Marcus\Application\ConsoleApplication;
use Marcus\Application\WebApplication;
use Marcus\Kernel;
use PHPUnit\Framework\TestCase;


/**
 * @package MarcusTest
 * @covers Marucs\Kernel
 */
class KernelTest extends TestCase
{
  public function testKernelCanLoadConfiguration()
  {
    $app_path = realpath(dirname(__DIR__).'/../');
    $kernel = new Kernel($app_path, true);
    $application = new WebApplication($kernel);
    $application->run();
    $this->assertTrue(is_array($kernel->getConfig()));
    $this->assertArrayHasKey('marcus',$kernel->getConfig());
  }
}
